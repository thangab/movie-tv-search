import { useEffect } from "react";
import { useDispatch } from "react-redux";

export const useFetching = (fetchAction, param) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchAction(param));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
};
