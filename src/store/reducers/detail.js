import { INIT_DETAIL } from "../actions";

const initialState = {};

const detail = (state = initialState, action) => {
  switch (action.type) {
    case INIT_DETAIL:
      return action.payload.data;
    default:
      return state;
  }
};

export default detail;
