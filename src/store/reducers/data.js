import {
  INIT_DATA,
  INIT_SEARCH_DATA,
  CLEAR_SEARCH,
  SET_QUERY,
} from "../actions";

const initialState = { default: {}, search: {}, query: "" };

const data = (state = initialState, action) => {
  switch (action.type) {
    case INIT_DATA:
      return { ...state, default: action.payload.data };
    case INIT_SEARCH_DATA:
      return { ...state, search: action.payload.data };
    case SET_QUERY:
      return { ...state, query: action.payload.query };
    case CLEAR_SEARCH:
      return { ...state, search: {}, query: "" };
    default:
      return state;
  }
};

export default data;
