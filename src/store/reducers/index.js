import { combineReducers } from "redux";
import data from "./data";
import detail from "./detail";

export default combineReducers({
  data,
  detail,
});
