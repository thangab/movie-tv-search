import { call, put, takeEvery, all } from "redux-saga/effects";
import {
  FETCH_DATA,
  FETCH_DETAIL,
  SEARCH_DATA,
  initData,
  initDetail,
  initSearchData,
} from "./actions";

const API_URL = "https://api.themoviedb.org/3/";
const API_KEY = "92b418e837b833be308bbfb1fb2aca1e";

export function* getDiscoverData() {
  try {
    const response = yield call(
      fetch,
      `${API_URL}discover/movie?api_key=${API_KEY}&language=en-US&sort_by=popularity.desc&page=1`
    );
    const data = yield call([response, "json"]);
    yield put(initData(data));
  } catch (e) {
    console.log("Error", e);
  }
}

export function* getDetail({ payload }) {
  const { type, id } = payload;
  try {
    const response = yield call(
      fetch,
      `${API_URL}${type}/${id}?api_key=${API_KEY}&language=en-US`
    );
    const data = yield call([response, "json"]);
    yield put(initDetail(data));
  } catch (e) {
    console.log("Error", e);
  }
}

export function* getSearchData({ payload }) {
  const { query } = payload;
  try {
    const response = yield call(
      fetch,
      `${API_URL}search/multi/?api_key=${API_KEY}&language=en-US&query=${query}&page=1&include_adult=false`
    );
    const data = yield call([response, "json"]);
    yield put(initSearchData(data));
  } catch (e) {
    console.log("Error", e);
  }
}

function* watchFetching() {
  yield takeEvery(FETCH_DATA, getDiscoverData);
  yield takeEvery(FETCH_DETAIL, getDetail);
  yield takeEvery(SEARCH_DATA, getSearchData);
}

export default function* watchSaga() {
  yield all([watchFetching()]);
}
