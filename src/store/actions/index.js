export const FETCH_DATA = "FETCH_DATA";
export const INIT_DATA = "INIT_DATA";
export const FETCH_DETAIL = "FETCH_DETAIL";
export const INIT_DETAIL = "INIT_DETAIL";
export const SEARCH_DATA = "SEARCH_DATA";
export const INIT_SEARCH_DATA = "INIT_SEARCH_DATA";
export const CLEAR_SEARCH = "CLEAR_SEARCH";
export const SET_QUERY = "SET_QUERY";

export const fetchData = () => ({ type: FETCH_DATA });

export const initData = (data) => ({ type: INIT_DATA, payload: { data } });

export const initSearchData = (data) => ({
  type: INIT_SEARCH_DATA,
  payload: { data },
});

export const searchData = (query) => {
  return {
    type: SEARCH_DATA,
    payload: { query },
  };
};

export const clearSearch = () => ({ type: CLEAR_SEARCH });

export const fetchDetail = (param) => ({
  type: FETCH_DETAIL,
  payload: { ...param },
});

export const initDetail = (data) => ({ type: INIT_DETAIL, payload: { data } });

export const setQuery = (query) => ({ type: SET_QUERY, payload: { query } });
