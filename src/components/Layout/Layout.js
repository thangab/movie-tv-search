import React from "react";
import AppBar from "@material-ui/core/AppBar";
import MovieIcon from "@material-ui/icons/Movie";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";

function Layout({ children }) {
  return (
    <>
      <AppBar position="relative">
        <Toolbar>
          <MovieIcon />
          <Typography variant="h6" color="inherit" noWrap>
            Movie / Tv App
          </Typography>
        </Toolbar>
      </AppBar>
      <Container component="main" maxWidth="md">
        {children}
      </Container>
    </>
  );
}

export default Layout;
