import React, { memo } from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { CardMovie } from "../../components/CardMovie";

function List({ title, data }) {
  return (
    <>
      <Typography gutterBottom variant="h4" component="h2">
        {title}
      </Typography>
      {data?.length > 0 ? (
        <Grid container spacing={4}>
          {data.map(
            ({ id, media_type, name, original_title, backdrop_path }) => (
              <Grid item key={id} xs={12} sm={6} md={4}>
                <CardMovie
                  type={media_type || "movie"}
                  id={id}
                  title={original_title || name}
                  imageUrl={backdrop_path}
                />
              </Grid>
            )
          )}
        </Grid>
      ) : (
        <Typography gutterBottom variant="h5" component="h2">
          No result
        </Typography>
      )}
    </>
  );
}

export default memo(List);
