import React from "react";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Chip from "@material-ui/core/Chip";

const useStyles = makeStyles((theme) => ({
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    paddingTop: "50%",
  },
  cardContent: {
    flexGrow: 1,
  },
}));

function CardMovie({ type, title, id, imageUrl }) {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      {imageUrl && (
        <CardMedia
          className={classes.cardMedia}
          image={`https://image.tmdb.org/t/p/w500${imageUrl}`}
          title={title}
        />
      )}
      <CardContent className={classes.cardContent}>
        <Typography gutterBottom variant="h6" component="h2">
          {title}
        </Typography>
        <Chip label={type} />
      </CardContent>
      <CardActions>
        <Link to={`/${type}/${id}`}>
          <Button size="small" color="primary">
            More details
          </Button>
        </Link>
      </CardActions>
    </Card>
  );
}

export default CardMovie;
