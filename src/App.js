import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Detail } from "./containers/Detail";
import { SearchList } from "./containers/SearchList";
import { Layout } from "./components/Layout";

function App() {
  return (
    <Router>
      <Layout>
        <Switch>
          <Route exact path="/" component={SearchList} />
          <Route path="/:type/:id" component={Detail} />
        </Switch>
      </Layout>
    </Router>
  );
}

export default App;
