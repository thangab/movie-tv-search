import React from "react";
import { fetchData } from "../../store/actions";
import { useFetching } from "../../hooks/useFetching";
import { useSelector } from "react-redux";
import { List } from "../../components/List";
import { SearchBar } from "../SearchBar";

function SearchList() {
  useFetching(fetchData);
  const data = useSelector(({ data }) => data);

  return (
    <>
      <SearchBar />
      {data?.search?.results && (
        <List title="Search movies" data={data?.search?.results} />
      )}
      <List title="Discover movies" data={data?.default?.results} />
    </>
  );
}

export default SearchList;
