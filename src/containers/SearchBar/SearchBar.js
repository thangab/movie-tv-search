import React, { useEffect } from "react";
import { searchData, clearSearch, setQuery } from "../../store/actions";
import { useDispatch, useSelector } from "react-redux";
import { useDebounce } from "use-debounce";
import { makeStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import FormControl from "@material-ui/core/FormControl";
import ClearIcon from "@material-ui/icons/Clear";
import SearchIcon from "@material-ui/icons/Search";

const useStyles = makeStyles((theme) => ({
  search: {
    display: "flex",
  },
  input: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(6),
    width: "100%",
  },
}));

function SearchBar() {
  const classes = useStyles();

  const dispatch = useDispatch();
  const query = useSelector((state) => state.data.query);
  const [debounceValue] = useDebounce(query, 800);

  useEffect(() => {
    if (debounceValue !== "") {
      dispatch(searchData(debounceValue));
    }
  }, [dispatch, debounceValue]);

  const onChangeValue = ({ currentTarget }) => {
    const { value } = currentTarget;
    dispatch(setQuery(value));
    if (value === "") {
      clearQuery();
    }
  };

  const clearQuery = () => {
    dispatch(clearSearch());
  };

  return (
    <div className={classes.search}>
      <FormControl className={classes.input}>
        <InputLabel htmlFor="searchbar">Search Movie / Tv</InputLabel>
        <Input
          id="searchbar"
          label="Search movie / TV"
          variant="outlined"
          onChange={onChangeValue}
          value={query}
          endAdornment={
            <InputAdornment position="end">
              <IconButton aria-label="Clear Search" onClick={clearQuery}>
                {query !== "" ? <ClearIcon /> : <SearchIcon />}
              </IconButton>
            </InputAdornment>
          }
        />
      </FormControl>
    </div>
  );
}

export default SearchBar;
