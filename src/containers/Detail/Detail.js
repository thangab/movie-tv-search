import React from "react";
import { useParams } from "react-router-dom";
import { fetchDetail } from "../../store/actions";
import { useFetching } from "../../hooks/useFetching";
import { useSelector } from "react-redux";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import { Link as LinkReact } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Chip from "@material-ui/core/Chip";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles((theme) => ({
  genre: {
    display: "flex",
    justifyContent: "center",
    flexWrap: "wrap",
    listStyle: "none",
    padding: theme.spacing(0.5),
    margin: 0,
  },
  chip: {
    margin: theme.spacing(0.5),
  },
}));

const Detail = () => {
  const classes = useStyles();

  const { type, id } = useParams();
  useFetching(fetchDetail, { type, id });
  const detail = useSelector(({ detail }) => detail);

  return (
    <>
      <LinkReact to="/">
        <Button variant="contained" color="primary">
          Back
        </Button>
      </LinkReact>
      {detail.original_title || detail.name ? (
        <Grid container spacing={4}>
          <Grid item xs={12} sm={6} md={6}>
            <img
              src={`https://image.tmdb.org/t/p/original${detail.poster_path}`}
              width="100%"
              alt={detail.original_title}
            />
          </Grid>
          <Grid item xs={12} sm={6} md={6}>
            <Typography component="h1" variant="h3">
              {detail.original_title || detail.name}
            </Typography>
            <Paper component="ul" className={classes.genre}>
              {detail?.genres.map((data) => {
                return (
                  <li key={data.id}>
                    <Chip label={data.name} className={classes.chip} />
                  </li>
                );
              })}
            </Paper>
            <Link href={detail.homepage}>{detail.homepage}</Link>
            <Typography component="p" variant="body2">
              {detail.overview}
            </Typography>
          </Grid>
        </Grid>
      ) : (
        <Typography component="h1" variant="h3">
          No detail found
        </Typography>
      )}
    </>
  );
};

export default Detail;
