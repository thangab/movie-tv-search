Ce projet a été bootstrappé avec [Create React App]

### `npm install`

Pour installer les dépendances

### `npm start`

Pour run l'application sur http://localhost:3000

### Projet

- Material-ui pour le design de l'application
- react-router-dom pour naviguer dans l'application (Page Principale et page Detail)
- Redux/React-Redux pour le store
- Redux-Saga pour le middleware, pour gérer tous les appels asynchrones
- Création d'un custom hooks "useFetching" pour l'initilisation des data, il n'est appelé qu'une seule fois (ComponentDidMount)
- Utilisation d'une lib "useDebounce" pour ne pas faire trop de call api lors de la recherche
- Utilisation de "React.memo" sur le composant List pour ne pas re-render lorsque les data ne changent pas (PureComponent)

Le fonctionement de l'application:

- Par défaut une liste de films est affiché (Discover movie)
- Taper dans le champ de recherche le titre du film ou de la série, une nouvelle liste va apparaitre avec les résultats de la recherche
- Cliquer sur l'icone X dans le champ de recherche pour effacer le recherche
- Cliquer sur "More details" pour aller sur la Page Detail, Revenir à la page précédente avec le bouton "Back"
